package cz.sedlackovi;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet("/report")
public class HelloWorldServlet extends HttpServlet {

     private static final long serialVersionUID = 1L;

     @Override
     protected void doGet(HttpServletRequest req, HttpServletResponse resp)  
              throws ServletException, IOException {
         Properties props = new Properties();
         props.load(new FileReader("/data/tomcat/config/settings.properties"));

         resp.getOutputStream().write(("Hello World from " + props.getProperty("system")).getBytes());
     }
}
