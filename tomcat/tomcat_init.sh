#!/bin/bash

# Generate password

PASS=${TOMCAT_PASS:-$(cat /dev/urandom| tr -dc 'a-zA-Z0-9' | fold -w 10| head -n 1)}
_word=$( [ ${TOMCAT_PASS} ] && echo "preset" || echo "random" )

echo "=> Creating and admin user with a ${_word} password in Tomcat"
sed -i -r 's/<\/tomcat-users>//' ${CATALINA_HOME}/conf/tomcat-users.xml
echo '<role rolename="manager-gui"/>' >> ${CATALINA_HOME}/conf/tomcat-users.xml
echo '<role rolename="manager-script"/>' >> ${CATALINA_HOME}/conf/tomcat-users.xml
echo '<role rolename="manager-jmx"/>' >> ${CATALINA_HOME}/conf/tomcat-users.xml
echo '<role rolename="admin-gui"/>' >> ${CATALINA_HOME}/conf/tomcat-users.xml
echo '<role rolename="admin-script"/>' >> ${CATALINA_HOME}/conf/tomcat-users.xml
echo "<user username=\"admin\" password=\"${PASS}\" roles=\"manager-gui,manager-script,manager-jmx,admin-gui, admin-script\"/>" >> ${CATALINA_HOME}/conf/tomcat-users.xml
echo '</tomcat-users>' >> ${CATALINA_HOME}/conf/tomcat-users.xml 
echo "=> Done!"

echo "========================================================================"
echo "You can now configure to this Tomcat server using:"
echo ""
echo "    admin:$PASS"
echo ""
echo "========================================================================"

MY_IP_ADDRESS=$(grep "$(hostname)" /etc/hosts | awk '{print $1}' | head -n 1)

echo "External IP address of the Tomcat host: $MY_IP_ADDRESS"

# function which gets called when this script received SIGTERM signal
# from Docker upon the container shutting down.
function stop_tomcat_sigterm()
{
    echo "Tomcat is about to stop"

    $CATALINA_HOME/bin/shutdown.sh

    EXIT_CODE=$?

    echo "Tomcat has stopped with the exit code $EXIT_CODE"

    exit $EXIT_CODE
}

trap stop_tomcat_sigterm SIGTERM

# Tomcat is likely to hang when it comes to deployment of Docker host manager in VirtualBox / virtual machine due to 
# low entropy. This is workaround how to increase the entropy so it does not hang.

echo "JAVA_OPTS=\"\$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom\"" >> $CATALINA_HOME/bin/setenv.sh
chmod +x $CATALINA_HOME/bin/setenv.sh

# The fact that jmxremote.port and rmi.port are same is curcial here.
# It is important to set java.rmi.server.hostname to the external address 

# JAVA_RMI_SERVER_HOSTNAME variable sets java.rmi.server.hostname property in CATALINA_OPTS
# This property has to be set to external IP address of the host a container runs in.
# In case this container is running in Docker machine and we want to connect to JMX from
# our local host where Docker machine is running, we have to set it to external IP of the 
# Docker machine. In case this container is running directly at local host, it has to 
# be set to external IP address of the container itself.

if [ -z $JAVA_RMI_SERVER_HOSTNAME ]; then
    JAVA_RMI_SERVER_HOSTNAME=$MY_IP_ADDRESS
fi

CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=$TOMCAT_DEBUG_PORT,server=y,suspend=n \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=$TOMCAT_JMX_PORT \
    -Dcom.sun.management.jmxremote.rmi.port=$TOMCAT_JMX_PORT \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Djava.rmi.server.hostname=$JAVA_RMI_SERVER_HOSTNAME \
    -Dspring.config.location=$SPRING_CONFIG_DIR"

export CATALINA_OPTS

echo "Waiting for all linked services to be up and running"

waitforservices -timeout=$WAIT_FOR_SERVICES_TIMEOUT

echo "Tomcat is about to start"

$CATALINA_HOME/bin/startup.sh

# Above "startup.sh" does not block so it would fall through
# and the container would stop itself. We are just looping here 
# indefinitely to basically wait until this script receives SIGTERM
# which triggers exit function stop_tomcat_sigterm.
# While this script loops, in the background Tomcat is running. 

touch $CATALINA_HOME/logs/catalina.out

# not putting this into the background will cause 
# that when Docker sends SIGTERM to this script,
# it will not be caught by trap function and exit code 
# will not be 0

tail -f $CATALINA_HOME/logs/catalina.out &

while :
do
    sleep 10
done
