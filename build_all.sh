#!/bin/bash


function get_image_id()
{
    docker images | grep $1 | tr -s " " | cut -d" " -f3 | head -n 1
}

function tag_image()
{
    image_id=$(get_image_id $1)
    docker tag $image_id $1:$2
}

docker build --force-rm=true -t base base
tag_image base latest
tag_image base 1.0

docker build --force-rm=true -t java java
tag_image java latest
tag_image java 8u66

docker build --force-rm=true -t tomcat tomcat
tag_image tomcat latest
tag_image tomcat 8.0.32

docker build --force-rm=true -t activemq activemq
tag_image activemq latest
tag_image activemq 5.12.3

docker build --force-rm=true -t mongodb mongodb
tag_image mongodb latest
tag_image mongodb 3.2.3

docker build --force-rm=true -t mariadb mariadb
tag_image mariadb latest
tag_image mariadb 10.1.14

docker build --force-rm=true -t webapp-base webapp-base
tag_image webapp-base latest
tag_image webapp-base 1.0
