#!/bin/sh

mysql_install_db --user mysql > /dev/null

echo "Starting temporary MariaDB instance for the initialization"

/etc/init.d/mysql start

# wait few secs to be sure

sleep 5

echo "Executing permissions script against temporary MariaDB instance"

mysql -u root -h 127.0.0.1 -P 3306 < /root/mariadb.sql

sleep 5

/etc/init.d/mysql stop
