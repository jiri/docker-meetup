#!/bin/sh

echo "Starting MariaDB process"

# this is wrapped in exec in order to be sure that it can receive Unix signals (SIGTERM)
# correctly so the exit status upon docker-compose stop process is correctly returned

exec "/usr/sbin/mysqld" \
     "--port=$MARIADB_PORT" \
     "--basedir=/usr" \
     "--datadir=/var/lib/mysql" \
     "--plugin-dir=/usr/lib64/mysql/plugin" \
     "--user=mysql" \
     "--general-log-file=$GENERAL_LOG_FILE" \
     "--log-error=$LOG_ERROR" \
     "--pid-file=localhost.localdomain.pid"
